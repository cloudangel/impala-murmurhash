/**
 * huize Service Inc
 * All Rights Reserved @2018
 */
package com.gitee.cloudangel.impala.hash.test;


import com.gitee.cloudangel.impala.hash.Murmur2Hash;
import org.junit.Assert;
import org.junit.Test;

/**
 * 描述:
 *
 * @author tianyuliang
 * @version $Id: Murmur2HashDetailTest.java, v0.1 2022/3/31
 */
public class Murmur2HashImpalaTest {

    @Test
    public void test_hash64() {
        long impalaHash = 6439493127771207189L;
        String value = "hello-world";
        long javaHash = Murmur2Hash.hash64(value);
        Assert.assertEquals(impalaHash, javaHash);
        System.out.println("value=" + value + ", javaHash=" + javaHash + ", impalaHash=" + impalaHash);
    }

    @Test
    public void test_hash64_case1() {
        long impalaHash = 18107030381288614L;
        String value = "www.baidu.com";
        long javaHash = Murmur2Hash.hash64(value);
        Assert.assertEquals(impalaHash, javaHash);
        System.out.println("value=" + value + ", javaHash=" + javaHash + ", impalaHash=" + impalaHash);
    }

    @Test
    public void test_hash64_case2() {
        long impalaHash = 2824456279270265267L;
        String value = "\n";
        long javaHash = Murmur2Hash.hash64(value);
        Assert.assertEquals(impalaHash, javaHash);
        System.out.println("value=" + value + ", javaHash=" + javaHash + ", impalaHash=" + impalaHash);
    }

}
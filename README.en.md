# impala murmur_hash

#### Description
CDH impala murmur_hash java impl


#### Instructions

```
# java 

long hashCode = Murmur2Hash.hash64("hello-world");

```


```
# impala  
select murmur_hash("hello-world") as hashCode ;

```



# impala murmur_hash

#### 介绍
CDH集群Impala服务`murmur_hash`算法java实现


#### 使用说明

```
# java

long hashCode = Murmur2Hash.hash64("hello-world");

```


```
# impala
select murmur_hash("hello-world") as hashCode ;

```
